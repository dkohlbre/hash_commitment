#!/usr/bin/python2

import hashlib
import random
import sys
import os

sep_string = ":::"
filename = os.path.expanduser("~/.commitments/statements.log")

def generate_nonce():
    # Seed with time or whatever, don't care, this isn't crypto
    random.seed()
    return "0x%0.16X" % random.randint(0,sys.maxint)

def hash_statement(statement,nonce):
    # Statement is just a string
    return hashlib.sha256(statement+sep_string+nonce).hexdigest()

def check_hash(string):
    nonce,statement,nshash_expected = string.split(sep_string)
    return (hash_statement(statement,nonce) == nshash_expected)

def record_statement(statement,nonce,nshash):
    # Make the dir if its not there

    if not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))

    with open(filename,'a') as f:
        f.write(nonce+sep_string+statement+sep_string+nshash+"\n\n")

def valid_statement(statement):
    return not (sep_string in statement)

def do_statement(statement):
    nonce = generate_nonce()
    if not valid_statement(statement):
        print "Invalid statement, statements cannot contain \""+sep_string+"\""
        return False

    nshash = hash_statement(statement,nonce)
    record_statement(statement,nonce,nshash)
    print "Statement \""+statement+"\" logged, post this: "+nshash
    return True


def smash_statements():
    if sys.argv[1] == "--check":
        return True,' '.join(sys.argv[2:])
    else:
        return False,' '.join(sys.argv[1:])


if(len(sys.argv) < 2):
    print "Usage:\n\t%s any statement\n\t%s --check full_log_string_to_check\n" % (sys.argv[0],sys.argv[0])
    exit(0)

checkstatement,statement = smash_statements()

if(checkstatement):
    print "looks good" if check_hash(statement) else "!!!HASH DOES NOT MATCH!!!"
else:
    do_statement(statement)
